﻿#pragma once
#include "token.h"

#include <cstdio>
#include <cctype>
#include <map>



namespace crud {

	using std::istream;
	using std::string;
	using std::map;

	constexpr int BUF_SIZE = 256;
	constexpr int MAX_OP_SIZE = 2;

	class lexer_error : std::exception {
	public:
		lexer_error(string _msg) : msg(_msg)
		{
		}
		~lexer_error() noexcept
		{
		}
		const char* what() const noexcept(false)
		{
			return msg.c_str();
		}
	private:
		string msg;
	};

	class Lexer
	{
	public:
		Lexer(istream& s) : stream(s)
		{
			line = 1;
			col = 1;
			//init_lookup();
		}

		bool is_op(char ch) const
		{
			return ch == '&' || ch == '|' || ch == '!' || ch == '<'
				|| ch == '>' || ch == '=' || ch == '+' || ch == '-'
				|| ch == '*' || ch == '/' || ch == '(' || ch == ')'
				|| ch == ',' || ch == ';';
		}

		expr_token next()
		{
			while (!is_end())
			{
				if (isdigit(peek))
				{  // numbers
					num_buffer = 0;
					do
					{
						num_buffer = num_buffer * 10 + consume() - '0';
					} while (isdigit(peek));

					return expr_token(NUM, &num_buffer, sizeof(int));

				}
				else if (isalpha(peek) || peek == '_')
				{  // ключевые слова или иденты
					memset(buffer, '\0', sizeof(buffer));
					int count = 0;
					do
					{
						buffer[count++] = consume();
					} while (count < BUF_SIZE && (isalnum(peek) || peek == '_'));

					if (count == BUF_SIZE)
					{
						throw lexer_error("Exceed maximun identifier length");
					}

					string str(buffer);
					for (size_t i = 0; i < str.size(); ++i)
					{
						str[i] = tolower(str[i]); // case insensitive
					}

					if (words.find(str) != words.end())
					{  // keyword
						return expr_token(words[str]);
					}
					else
					{
						str = buffer;  // case sensitive for identifier
						return expr_token(ID, str.c_str(), str.size());
					}
				}
				else if (isspace(peek))
				{  // пробел
					if (consume() == '\n')
					{
						line++;
						col = 1;
					}
				}
				else if (single_op.find(peek) != single_op.end())
				{
					// односимвольный оператор? 
					return expr_token(single_op[consume()]);

				}
				else if (is_op(peek))
				{  // операторы другие
					memset(buffer, '\0', sizeof(buffer));
					int count = 0;
					do
					{
						buffer[count++] = consume();
					} while (is_op(peek) && count << MAX_OP_SIZE);

					string str(buffer);

					while (str.size() != 0 && ops.find(str) == ops.end())
					{ // too long
						char temp = str[str.size() - 1]; // last charactor
						str = str.substr(0, str.size() - 1);
						retreat(temp);
					}

					if (str.size() == 0)
					{
						throw lexer_error("Invalid operator");
					}

					return expr_token(ops[str]);
				}
				else
				{  // error
					string msg = "Invalid lexeme ";
					msg.push_back(consume());
					throw lexer_error(msg);
				}
			}

			// peek == EOF
			consume();
			return expr_token(END);  // $
		}

		int get_column() const
		{
			return col;
		}

		int get_line() const
		{
			return line;
		}

		// читать больше нечего, консьюмим peek == EOF
		bool is_end()
		{
			peek = stream.peek();
			return peek == EOF;
		}

		// движемся по потоку, возвращаем прочитанные символы
		char consume()
		{
			char last = stream.get();
			peek = stream.peek();
			col++;  
			return last;
		}
		
		// вернуть символ в поток
		void retreat(char ch)
		{  
			stream.putback(ch);
			col--;  
			peek = ch;
		}

		// Ключевые слова
		map<string, token_type> words =
		{
			{"create" ,  CREATE},
			{"table" ,  TABLE},
			{"int" ,  INT},
			{"default" ,  DEFAULT},
			{"primary" ,  PRIMARY},
			{"key" ,  KEY},
			{"insert" ,  INSERT},
			{"into" ,  INTO},
			{"values" ,  VALUES},
			{"delete" ,  DELETE},
			{"from" ,  FROM},
			{"where" ,  WHERE},
			{"select" ,  SELECT }
		};
		// Односимвольные операторы
		map<char, token_type> single_op =
		{
			{'+', PLUS},
			{'-' , MINUS},
			{'*' , MUL},
			{'/' , DIV},
			{'(' , L_PAREN},
			{')' , R_PAREN},
			{',' , COMMA},
			{';' , SEMICOLON}
		};
		map<string, token_type> ops=
		{
			{"&&" , AND},
			{"||" , OR},
			{"!" , NOT},
			{"<" , LT},
			{">" , GT},
			{"<>" , NEQ},
			{"," , ASSIGN},
			{",," , EQ},
			{">," , GEQ},
			{"<," , LEQ},
			{"+" , PLUS},
			{"-" , MINUS},
			{"*" , MUL},
			{"/" , DIV},
			{"(" , L_PAREN},
			{")" , R_PAREN},
			{"," , COMMA},
			{";" , SEMICOLON},
		};  // Все остальные операторы
	private:

		char peek;  // символ, который идет за текущим для advance()
		char buffer[BUF_SIZE];  // должно хватить, ключевые слова, операторы и тп
		int num_buffer;  // для накопления чисел
		int line;  // строка
		int col; // столбец
		istream& stream;  // поток, который консюмит лексер
	};

}
