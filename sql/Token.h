﻿#pragma once 
#include <string>
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <map>


namespace crud
{

	using std::string;
	using std::ostream;
	using std::map;

	// Ключевые слова и операторы
	enum token_type 
	{
		ID, NUM, CREATE, TABLE, INT, DEFAULT, PRIMARY, KEY, INSERT,
		INTO, VALUES, DELETE, FROM, WHERE, SELECT, ASSIGN, LT, GT, NEQ, EQ,
		GEQ, LEQ, PLUS, MINUS, MUL, DIV, AND, OR, NOT, L_PAREN, R_PAREN,
		COMMA, SEMICOLON, END, NONE
	};

	class token_error : std::exception 
	{
	public:
		token_error(string _msg) : msg(_msg) {}
		~token_error() noexcept
		{} 

		const char* what() const noexcept 
		{
			return msg.c_str(); 
		}

	private:
		string msg;
	};

	/* Терминалы и токенизация.
	 *
	 * 
	 * 4 вида токенов:
	 *     1. Ключевые слова. `type` собсно тип (например Type::CREATE), `data`=nullptr,
	 *        `real_size`=0. `Type getKeyword()` Для получения.
	 *     2. Оператор.  `type` собсно тип (например CREATE), `data`=nullptr,
	 *        `real_size`=0. `Type getKeyword()` Для получения.
	 *     3. Идентификатор. `type` тип Type::ID, `data` содержит сырые строковые данные, `real_size` длина строки + 1(для терминатора '\0').
	 *         `string getId()` Для получения..
	 *     4. Число. `type` тип Type::NUM, `data` содержит целое число,
	 *         `real_size` равен размеру инта.
	 *          `int getNumber()` Для получения.
	 */
	class expr_token 
	{
	public:
		expr_token(token_type _type = NONE, const void* raw = nullptr, size_t size = 0)
		{
			if (is_value(_type))
			{  // number || identifier
				if (raw == nullptr || size == 0)
				{
					throw token_error("Expected non-keyword");
				}
				else
				{
					type = _type;

					real_size = size / sizeof(char);
					if (type == ID)
					{
						real_size++;  // '\0'
					}

					data = new char[real_size];
					memcpy(data, (char*)raw, real_size);
				}
			}
			else
			{  // keyword || operator
				type = _type;
				data = nullptr;
				real_size = 0;
			}
		}

		expr_token(const expr_token& other)
		{
			type = other.type;
			if (is_value(type))
			{  // number || identifier
				real_size = other.real_size;
				data = new char[real_size];
				memcpy(data, other.data, real_size);
			}
			else
			{  // keyword || operator
				real_size = 0;
				data = nullptr;
			}
		}

		// Номер идентификатора
		bool is_value(token_type t) const // число или идентификатор
		{  
			return t == NUM || t == ID;
		}

		token_type get_type() const 
		{
			return type;
		}

		// для идентов. чекать по соответствию get_id() == ID
		string get_id() const
		{
			if (type != ID)
			{
				throw token_error("Expected identifer, get otherwise");
			}
			else
			{
				return string(data);
			}
		}

		// Для ключевых слов и операторов. чекать !is_value(get_type())
		token_type get_keyword() const
		{
			if (is_value(type))
			{
				throw token_error("Expected keywords, get otherwise");
			}
			else
			{
				return type;
			}
		}

		// Для чисел, чекать по get_type() == NUM
		int get_number() const
		{
			if (type != NUM)
			{
				throw token_error("Expected number, get otherwise");
			}
			else
			{
				int result = 0;
				memcpy(&result, data, sizeof(int));
				return result;
			}
		}

	
		bool operator== (token_type rhs) const {
			return type == rhs;
		}
		bool operator!= (token_type rhs) const {
			return !(*this == rhs);
		}

		expr_token& operator=(const expr_token& rhs)
		{
			type = rhs.type;

			// старые данные удалят
			if (data != nullptr)
			{
				delete[] data;
				data = nullptr;
				real_size = 0;
			}

			if (is_value(type))
			{  // number || identifier
				real_size = rhs.real_size;
				data = new char[real_size];
				memcpy(data, rhs.data, real_size);
			}
			else
			{  // keyword || operator
				real_size = 0;
				data = nullptr;
			}
			return *this;
		}

		friend ostream& operator<<(ostream& s, const expr_token& token)
		{
			if (expr_token::name.size() == 0)
			{
				expr_token::runtime_init();
			}

			token_type type = token.type;
			string token_name = expr_token::name[type];

			if (type == ID)
			{
				s << "(" << token_name << ", " << token.get_id() << ")";
			}
			else if (type == NUM)
			{
				s << "(" << token_name << ", " << token.get_number() << ")";
			}
			else
			{
				s << token_name;
			}
			return s;
		}

		~expr_token()
		{
			if (data != nullptr)
			{
				delete[] data;
				data = nullptr;
			}
		}

		static map<token_type, string> name;   

	private:
		static void runtime_init()  // Инициализнуть статически при запуске :( 
		{
			name[ID] = "ID";
			name[NUM] = "NUM";
			name[CREATE] = "CREATE";
			name[TABLE] = "TABLE";
			name[INT] = "INT";
			name[DEFAULT] = "DEFAULT";
			name[PRIMARY] = "PRIMARY";
			name[KEY] = "KEY";
			name[INSERT] = "INSERT";
			name[INTO] = "INTO";
			name[VALUES] = "VALUES";
			name[DELETE] = "DELETE";
			name[FROM] = "FROM";
			name[WHERE] = "WHERE";
			name[SELECT] = "SELECT";
			name[ASSIGN] = "ASSIGN";
			name[LT] = "LT";
			name[GT] = "GT";
			name[NEQ] = "NEQ";
			name[EQ] = "EQ";
			name[GEQ] = "GEQ";
			name[LEQ] = "LEQ";
			name[PLUS] = "PLUS";
			name[MINUS] = "MINUS";
			name[MUL] = "MUL";
			name[DIV] = "DIV";
			name[AND] = "AND";
			name[OR] = "OR";
			name[NOT] = "NOT";
			name[L_PAREN] = "L_PAREN";
			name[R_PAREN] = "R_PAREN";
			name[COMMA] = "COMMA";
			name[SEMICOLON] = "SEMICOLON";
			name[END] = "END";
			name[NONE] = "NONE";
		}

		token_type type;  // Тип токена
		char* data;  // данные из потока
		int real_size;  // размер данных
	};

}

