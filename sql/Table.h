﻿#pragma once
#include <set>
#include <map>
#include <string>
#include <vector>
#include <algorithm>
#include "parse_tree.h"


namespace crud {

	using std::set;
	using std::map;
	using std::string;
	using std::vector;
	using std::to_string;

	class db_error : std::exception
	{
	public:
		db_error(string _msg) : msg(_msg) {}

		~db_error() noexcept {}

		const char* what() const noexcept
		{
			return msg.c_str();
		}
	private:
		string msg;
	};

	/**
	 * —хема Ѕƒ.
	 */
	struct Scheme
	{
		string name;
		int def;
		bool is_key;
		Scheme(string _name, int _def, bool _is_key)
			: name(_name), def(_def), is_key(_is_key) {}
	};

	/**
	 * ”правл¤ем транзакци¤ми.
	 * „екаем семантику по схеме.
	 */
	class Table 
	{
	public:
		Table() = default;
		

		// конструирует табличку с id, default mappings, primary key
		Table(const string table_id, const map<string, int> defs,
			const vector<string> primary)
		{
			int counter = 0;
			for (auto it = defs.begin(); it != defs.end(); ++it)
			{
				Scheme new_scheme = Scheme(it->first, it->second,
					keyfields.find(it->first) != keyfields.end());
				indexes[it->first] = counter++;
				schema.push_back(new_scheme);
				columns.push_back(it->first);
			}
		}
		
		// —обсно вставка значений
		Table& insert(const vector<string> cols, const vector<int> values)
		{
			// проверить констрейнты первичного ключа
			set<string> colset(cols.begin(), cols.end());
			for (const auto& key : keyfields)
			{
				if (colset.find(key) == colset.end())
				{
					throw db_error(string("Key ") + key + string(" not found"));
				}
			}


			for (auto col : colset)
			{
				if (std::find(columns.begin(), columns.end(), col) == columns.end())
				{
					throw db_error(string("Column ") + col
						+ string(" is not in the schema"));
				}
			}

			vector<int> new_record(columns.size());
			// заполним дефолтными значени¤ми
			for (size_t i = 0; i < columns.size(); ++i)
			{
				new_record[i] = schema[i].def;
			}


			for (size_t i = 0; i < cols.size(); ++i)
			{
				int index = indexes[cols[i]];
				new_record[index] = values[i];
			}


			for (const auto& record : data)
			{
				if (has_conflict(record, new_record))
				{
					throw db_error(string("Record already exists"));
				}
			}

			data.push_back(new_record);
			return *this;
		}

		// удаление строки, удовлетвор¤ющей выражению
		int del(const parse_tree expr)
		{
			if (data.size() <= 0)
			{
				return 0;
			}

			auto it = data.begin();
			int count = 0;

			// столбец из where должен быть в схеме
			while (it != data.end())
			{
				if (expr.eval(*it, indexes))
				{
					it = data.erase(it);
					count++;
				}
				else
				{
					it++;
				}
			}

			return count;
		}

		// запрос строки, удовлетвор¤ющей выражению
		int query(const vector<string>& names, const parse_tree expr, vector<vector<int> >& results) const
		{
			int count = 0;
			if (data.size() <= 0)
			{
				return 0;
			}


			if (std::find(names.begin(), names.end(), "*") != names.end()) { // all
				for (const auto& record : data)
				{
					if (expr.eval(record, indexes))
					{
						// столбец из where должен быть в схеме
						results.push_back(record);
						count++;
					}
				}
				return count;
			}
			else
			{

				for (const auto& name : names)
				{
					if (std::find(columns.begin(), columns.end(), name)
						== columns.end())
					{
						throw db_error(string("Column ") + name
							+ string(" is not in the schema"));
					}
				}

				//индекс дл¤ переупор¤дочивани¤
				vector<int> query_indexes;
				for (const auto& name : names)
				{
					auto it = indexes.find(name);
					query_indexes.push_back(it->second);
				}

				for (const auto& record : data)
				{
					if (expr.eval(record, indexes))
					{

						vector<int> reordered;
						for (const auto& idx : query_indexes)
						{
							reordered.push_back(record[idx]);
						}
						results.push_back(reordered);
						count++;
					}
				}

				return count;
			}

		}

		// список столбцов таблицы
		const vector<string>& get_columns() const {
			return columns;
		}
		
		// чекаем конфликты
		bool has_conflict(const vector<int> old_record, const vector<int> new_record) const
		{
			for (const auto& key : keyfields)
			{
				auto it = indexes.find(key);
				int idx = it->second;
				if (old_record[idx] != new_record[idx])
				{
					return false;
				}
			}
			return true;
		}

		~Table() = default;
	private:
		string id;

		set<string> keyfields;  // первичный ключ
		map<string, int> indexes;  // маппит имена в индексы

		//пор¤док важен!
		vector<Scheme> schema;
		vector<string> columns;  // имена столбцов
		vector<vector<int> > data;
	};

}
