﻿#pragma once
#include "table.h"
#include "statement.h"


constexpr int MAX_COL = 100;
constexpr int MAX_KEY = 100;

namespace crud {

	/**
	 * Проводит семантический анализ
	 * На схему не смотрим
	 * мониторинг состояния таблицы
	 * извелечение компонент выражения и передача их таблице (где схема нужна)
	 */
	class db_engine {
	public:
		db_engine() {}
		bool create_(const Create& create_stmt)
		{
			// сначала проверить что таблица не создана ранее
			string table_id = create_stmt.getId();
			if (user_tables.find(table_id) != user_tables.end()) {
				throw db_error(string("Table ") + table_id
					+ string(" already exists"));
			}

			// проверить что первичный ключ один
			const vector<vector<string> >& keys = create_stmt.get_keys();
			if (keys.size() > 1)
			{
				throw db_error("Multiple primary key definitions");
			}

			// дубликаты столбцов
			const multimap<string, int>& defs = create_stmt.get_defaults();
			map<string, int> unique_defs;
			for (auto it = defs.begin(); it != defs.end();
				it = defs.upper_bound(it->first))
			{
				if (defs.count(it->first) > 1)
				{
					throw db_error(string("Multiple definitions for ")
						+ it->first);
				}
				else
				{
					unique_defs[it->first] = it->second;
				}
			}

			// все первичные ключы определены
			vector<string> primary;
			if (keys.size() > 0)
			{
				primary = keys[0];
				for (const auto& key : primary)
				{
					if (defs.find(key) == defs.end())
					{
						throw db_error(string("Undefined key ") + key);
					}
				}
			}
			else
			{  // no keys
				for (const auto& kv : defs)
				{
					primary.push_back(kv.first);
				}
			}


			if (unique_defs.size() > MAX_COL)
			{
				throw db_error(string("Number of columns should be no more than ")
					+ to_string(MAX_COL));
			}

			if (primary.size() > MAX_KEY)
			{
				throw db_error(string("Number of keys should be no more than ")
					+ to_string(MAX_KEY));
			}

			Table new_table = Table(table_id, unique_defs, primary);
			user_tables[table_id] = new_table;
			return true;
		}

		// вставка записи
		bool insert_(const Insert& insert_stmt)
		{
			//  табличка должна существовать

			string table_id = insert_stmt.getId();
			auto it = user_tables.find(table_id);
			if (it == user_tables.end())
			{
				throw db_error(string("Cannot find table ") + table_id);
			}

			set<string> checked;
			const vector<string>& columns = insert_stmt.get_columns();
			const vector<int>& values = insert_stmt.get_values();

			// число столбцов соотвествует число данных значений
			if (columns.size() != values.size())
			{
				throw db_error("Numbers of columns and values does not match");
			}

			// нет повторяющихся столбцов
			for (const auto& col : columns)
			{
				if (checked.find(col) != checked.end())
				{
					throw db_error(string("Duplicate column ") + col);
				}
				else
				{
					checked.insert(col);
				}
			}

			// нет нарушений по первичному ключу
			// столбцы должны быть в схеме 
			it->second.insert(columns, values);
			return true;
		}

		// удаление записи
		int delete_(const Delete& delete_stmt)
		{
			string table_id = delete_stmt.getId();
			auto it = user_tables.find(table_id);
			if (it == user_tables.end())
			{
				throw db_error(string("Cannot find table ") + table_id);
			}

			//Столбец из where должен быть в схеме таблицы
			return it->second.del(delete_stmt.get_where());
		}

		// собсно запрос
		int query_(const Query& query_stmt, vector<vector<int> >& results) const
		{
			string table_id = query_stmt.getId();
			auto it = user_tables.find(table_id);
			if (it == user_tables.end())
			{
				throw db_error(string("Cannot find table ") + table_id);
			}

			//Столбцы должны существовать (TODO: SELECT *)
			return it->second.query(query_stmt.get_columns(), query_stmt.get_where(), results);
		}

		// список полей таблицы по ее айдишнику
		const vector<string>& get_columns(string table_id) const
		{
			auto it = user_tables.find(table_id);
			if (it == user_tables.end())
			{
				throw db_error(string("No table ") + table_id);
			}

			return it->second.get_columns();
		}

		~db_engine() = default;
	private:
		map<string, Table> user_tables;
	};

}
