﻿#pragma once

#include "token.h"
#include <vector>
#include <map>
#include <string>
#include <iostream>


namespace crud {

	using std::vector;
	using std::string;
	using std::map;


	class parse_tree_error : std::exception {
	public:
		parse_tree_error(string _msg) : msg(_msg) 
		{
		}

		~parse_tree_error() noexcept
		{
		}

		const char* what() const noexcept(false)
		{
			return msg.c_str();
		}
	private:
		string msg;
	};

	/* Класс управления выражениями. Реализует дерево разбора
	 *
	 * Три типа выражений???
	 *     1. Бинарные. `left` и `right` указывают на левое и правое выражения, `type` это оператор,
	 *        `value` это токен типа `NONE`.
	 *     2. Унарные. `left` м.б. nullptr, `right` это правосторонний операнд,
	 *        `type` унарный оператор, `value` это токен типа `NONE`.
	 *     3. Операнд. `type` это терминал ID или NUM, `left` и `right` оба  nullptr,
	 *         `value` токен для текущего операнда.
	 */
	/*expressions */
	class parse_tree
	{
	public:
		parse_tree(token_type t = NONE) : left(nullptr), right(nullptr), val(NONE), type(t) 
		{}
		parse_tree(const parse_tree& other)
		{
			val = other.val;
			type = other.type;
			left = right = nullptr;

			if (other.left != nullptr)
			{
				set_left(*(other.left));
			}
			else
			{
				left = nullptr;
			}

			if (other.right != nullptr)
			{
				set_right((*other.right));
			}
			else
			{
				right = nullptr;
			}
		}

		void set_left(const parse_tree& other)
		{
			if (left != nullptr)
			{
				delete left;
				left = nullptr;
			}

			left = new parse_tree(other.type);
			*left = other;
		}

		void set_right(const parse_tree& other)
		{
			if (right != nullptr)
			{
				delete right;
				right = nullptr;
			}

			right = new parse_tree(other.type);
			*right = other;
		}
		
		// помещает остальное в слева от крайнего слева выражения в дереве
		void set_leftmost(const parse_tree& other) 
		{
			parse_tree* cur = this;
			while (cur->has_left())
			{
				cur = cur->left;
			}
			cur->set_left(other);
		}

		void set_val(expr_token v) {
			val = v;
		}
		void set_type(token_type t) {
			type = t;
		}
		bool is_null() const {
			return type == NONE;
		}

		bool has_left() const {
			return left != nullptr;
		}

		bool has_right() const {
			return right != nullptr;
		}

		const parse_tree& get_right() const {
			return *right;
		}

		const parse_tree& get_left() const {
			return *left;
		}

		token_type get_type() const {
			return type;
		}

		bool is_val() const {
			return type == ID || type == NUM;
		}

		expr_token get_val() const {
			return val;
		}

		parse_tree& operator=(const parse_tree& rhs)
		{
			if (rhs.left != nullptr)
			{
				set_left(*(rhs.left));
			}
			else
			{
				if (left != nullptr)
				{
					delete left;
					left = nullptr;
				}
			}

			if (rhs.right != nullptr)
			{
				set_right((*rhs.right));
			}
			else
			{
				if (right != nullptr)
				{
					delete right;
					right = nullptr;
				}
			}

			val = rhs.val;
			type = rhs.type;
			return *this;
		}
		friend ostream& operator<<(ostream& s, const parse_tree& expr)
		{
			if (expr.is_val())
			{  // айди или число
				s << expr.get_val();
				return s;
			}

			// real expression
			s << "(";
			if (expr.has_left()) {
				s << expr.get_left();
			}

			if (!expr.is_null())
			{  // operator
				s << " " << expr_token(expr.get_type()) << " ";
			}

			if (expr.has_right()) {
				s << expr.get_right();
			}

			s << ")";
			return s;
		}

		// pass in the row record and id-to-index map
		// например eval([1,2,3], {"sid": 0, "age": 1, "name": 2})
		int eval(vector<int> record, map<string, int> indexes) const
		{

			/*switch (type)
			{
			case sql::ID:
				break;
			case sql::NUM:
				return value.getNumber();
				break;
			case sql::CREATE:
				break;
			case sql::TABLE:
				break;
			case sql::INT:
				break;
			case sql::DEFAULT:
				break;
			case sql::PRIMARY:
				break;
			case sql::KEY:
				break;
			case sql::INSERT:
				break;
			case sql::INTO:
				break;
			case sql::VALUES:
				break;
			case sql::DELETE:
				break;
			case sql::FROM:
				break;
			case sql::WHERE:
				break;
			case sql::SELECT:
				break;
			case sql::ASSIGN:
				break;
			case sql::LT:
				break;
			case sql::GT:
				break;
			case sql::NEQ:
				break;
			case sql::EQ:
				break;
			case sql::GEQ:
				break;
			case sql::LEQ:
				break;
			case sql::PLUS:
				break;
			case sql::MINUS:
				break;
			case sql::MUL:
				break;
			case sql::DIV:
				break;
			case sql::AND:
				break;
			case sql::OR:
				break;
			case sql::NOT:
				break;
			case sql::L_PAREN:
				break;
			case sql::R_PAREN:
				break;
			case sql::COMMA:
				break;
			case sql::SEMICOLON:
				break;
			case sql::END:
				break;
			case sql::NONE:
				break;
			default:
				break;
			}*/

			if (type == NUM)
			{
				return val.get_number();
			}
			else if (type == ID)
			{
				string id = val.get_id();
				auto it = indexes.find(id);
				if (it == indexes.end())
				{
					throw parse_tree_error(id + " not found in the scheme");
				}
				return record[it->second];
			}
			else if (type == AND)
			{
				return left->eval(record, indexes) && right->eval(record, indexes);
			}
			else if (type == EQ)
			{
				return left->eval(record, indexes) == right->eval(record, indexes);
			}
			else if (type == NEQ)
			{
				return left->eval(record, indexes) != right->eval(record, indexes);
			}
			else if (type == LT)
			{
				return left->eval(record, indexes) < right->eval(record, indexes);
			}
			else if (type == GT)
			{
				return left->eval(record, indexes) > right->eval(record, indexes);
			}
			else if (type == LEQ)
			{
				return left->eval(record, indexes) <= right->eval(record, indexes);
			}
			else if (type == GEQ)
			{
				return left->eval(record, indexes) >= right->eval(record, indexes);
			}
			else if (type == PLUS)
			{
				if (has_left())
				{
					return left->eval(record, indexes) + right->eval(record, indexes);
				}
				else {
					return right->eval(record, indexes);
				}
			}
			else if (type == MINUS)
			{
				if (has_left())
				{
					return left->eval(record, indexes) - right->eval(record, indexes);
				}
				else
				{
					return 0 - right->eval(record, indexes);
				}
			}
			else if (type == MUL)
			{
				return left->eval(record, indexes) * right->eval(record, indexes);
			}
			else if (type == DIV)
			{
				int left_result = left->eval(record, indexes);
				int right_result = right->eval(record, indexes);
				if (right_result == 0)
				{
					throw parse_tree_error("Division by zero");
				}
				return left_result / right_result;
			}
			else if (type == OR)
			{
				return left->eval(record, indexes) || right->eval(record, indexes);
			}
			else if (type == NOT)
			{
				return !right->eval(record, indexes);
			}
			else if (type == NONE)
			{
				return true;  // для пустого where
			}
			return 0;
		}

		// для простых выражений без связывания (по id)
		int eval() const
		{
			vector<int> record;
			map<string, int> indexes;
			return eval(record, indexes);  // пустышка
		}

		~parse_tree()
		{
			if (left != nullptr)
			{
				delete left;
				left = nullptr;
			}

			if (right != nullptr)
			{
				delete right;
				right = nullptr;
			}
		}
	private:
		parse_tree* left;
		parse_tree* right;
		expr_token val;
		token_type type;
	};

	const parse_tree null_expression;

}
