﻿#pragma once
#include "lexer.h"
#include "parse_tree.h"
#include "statement.h"

namespace crud
{

	class parse_error : std::exception
	{
	public:
		parse_error(string _msg) : msg(_msg) {}
		~parse_error() noexcept {}
		const char* what() const noexcept
		{
			return msg.c_str();
		}
	private:
		string msg;
	};

	/* Предсказвыающий синтаксический анализатор.
	 *
	 * TODO: Проверить RVO или мув семантику
	 * Usage:
	 *     Parser parser(lexer);

	 *     // Поддержка CREATE/DELETE/INSERT/SELECT
	 *     Type next_type = parser.next_stmt_type();
	 *     if (next_type == CREATE) {  // Сначала проверяем типа
	 *         Create stmt = parser.create_stmt();  // потом получаем выражение
	 *     } // и т.д.
	 */
	class Parser
	{
	public:
		Parser(Lexer& l) : lexer(l)
		{
			col = lexer.get_column();
			line = lexer.get_line();
			lookahead = lexer.next();
			delay = false;
		}

		token_type check_next_statement()  // head of next statement
		{
			if (match(CREATE) || match(INSERT)
				|| match(DELETE) || match(SELECT))
			{
				return lookahead.get_type();
			}
			else if (match(END))
			{
				return END;
			}
			else
			{
				throw parse_error(string("Syntax error: statements should start with")
					+ string(" CREATE, INSERT, DELETE or SELECT")
					+ string("\n e.g. \"SELECT * FROM table_name;\"\nor \"CREATE TABLE table_name(sid INT,\nage INT DEFAULT = 18,\nPRIMARY KEY(sid),\nage INT);\""));
			}
		}

		Create create_statement()
		{
			if (match(CREATE))
			{
				// create_stmt ->  CREATE TABLE id L_PAREN decl_list
				//                 R_PAREN SEMICOLON
				consume(CREATE);
				consume(TABLE);
				string table_id = id();
				consume(L_PAREN);
				multimap<string, int> defaults;
				vector<vector<string> > keys;
				decl_list(defaults, keys);
				consume(R_PAREN);
				consume(SEMICOLON);
				return Create(table_id, defaults, keys);
			}
			else
			{
				throw parse_error("Syntax error");
			}
		}


		Parser& decl_list(multimap<string, int>& defaults, vector<vector<string> >& keys)
		{
			if (match(ID) || match(PRIMARY))
			{
				// decl_list -> decl _decl_list
				decl(defaults, keys);
				_decl_list(defaults, keys);
			}
			else
			{
				throw parse_error("Syntax error");
			}
			return *this;
		}

		Parser& _decl_list(multimap<string, int>& defaults, vector<vector<string> >& keys)
		{
			if (match(COMMA))
			{
				// _decl_list -> COMMA decl _decl_list
				consume(COMMA);
				decl(defaults, keys);
				_decl_list(defaults, keys);
			}
			else if (match(R_PAREN))
			{
				; // _decl_list -> epsilon
			}
			else
			{
				throw parse_error("Syntax error");
			}
			return *this;
		}

		Parser& decl(multimap<string, int>& defaults, vector<vector<string> >& keys)
		{
			if (match(ID))
			{
				// decl -> id INT default_spec
				string name = id();
				consume(INT);
				int number = default_spec();
				defaults.insert(std::pair<string, int>(name, number));
			}
			else if (match(PRIMARY))
			{
				// decl -> PRIMARY KEY L_PAREN column_list R_PAREN
				consume(PRIMARY);
				consume(KEY);
				consume(L_PAREN);
				vector<string> columns;
				column_list(columns);
				keys.push_back(columns);
				consume(R_PAREN);
			}
			else
			{
				throw parse_error("Syntax error");
			}
			return *this;
		}

		int default_spec()
		{
			if (match(DEFAULT))
			{
				// default_spec -> DEFAULT ASSIGN expr[true]
				consume(DEFAULT);
				consume(ASSIGN);
				parse_tree spec = expr(true);
#ifdef DEBUG
				std::cout << '\n' << spec << '\n';
#endif
				return spec.eval();
			}
			else if (match(COMMA) || match(R_PAREN))
			{
				// default_spec -> epsilon
				return 0;  // if no default, default to zero
			}
			else
			{
				throw parse_error("Syntax error");
			}
		}
		Parser& column_list(vector<string>& names)
		{
			if (match(ID))
			{
				// column_list ->  id _column_list
				names.push_back(id());
				_column_list(names);
			}
			else
			{
				throw parse_error("Syntax error");
			}
			return *this;
		}
		Parser& _column_list(vector<string>& names)
		{
			if (match(COMMA))
			{
				// _column_list ->  COMMA id _column_list
				consume(COMMA);
				names.push_back(id());
				_column_list(names);
			}
			else if (match(FROM) || match(R_PAREN))
			{
				;  // _column_list ->  epsilon
			}
			else
			{
				throw parse_error("Syntax error");
			}
			return *this;
		}

		Insert insert_statement()
		{
			if (match(INSERT))
			{
				// insert_stmt -> INSERT INTO id L_PAREN column_list R_PAREN
				//                VALUES L_PAREN value_list R_PAREN SEMICOLON
				consume(INSERT);
				consume(INTO);
				string table_id = id();
				consume(L_PAREN);
				vector<string> columns;
				column_list(columns);
				consume(R_PAREN);
				consume(VALUES);
				consume(L_PAREN);
				vector<int> values;
				value_list(values);
				consume(R_PAREN);
				consume(SEMICOLON);
				return Insert(table_id, columns, values);
			}
			else
			{
				throw parse_error("Syntax error");
			}
		}
		Parser& value_list(vector<int>& values)
		{
			if (match(PLUS) || match(MINUS)
				|| match(NUM) || match(L_PAREN)) {
				// value_list -> expr[true] _value_list
				parse_tree value = expr(true);
#ifdef DEBUG
				std::cout << '\n' << value << '\n';
#endif
				values.push_back(value.eval());
				_value_list(values);
			}
			else
			{
				throw parse_error("Syntax error");
			}
			return *this;
		}
		Parser& _value_list(vector<int>& values)
		{
			if (match(COMMA))
			{
				// _value_list -> COMMA expr[true] _value_list
				consume(COMMA);
				parse_tree value = expr(true);
#ifdef DEBUG
				std::cout << '\n' << value << '\n';
#endif
				values.push_back(value.eval());
				_value_list(values);
			}
			else if (match(R_PAREN))
			{
				;  // _value_list ->  epsilon
			}
			else
			{
				throw parse_error("Syntax error");
			}
			return *this;
		}

		Delete delete_statement()
		{
			if (match(DELETE))
			{
				// delete_stmt -> DELETE FROM id where_clause SEMICOLON
				consume(DELETE);
				consume(FROM);
				string table_id = id();
				parse_tree where = where_clause();
				consume(SEMICOLON);
				return Delete(table_id, where);
			}
			else
			{
				throw parse_error("Syntax error");
			}
		}
		
		//TODO: семантику перемещения вместо рво  
		parse_tree where_clause()
		{
			if (match(WHERE))
			{
				// where_clause -> WHERE disjunct
				consume(WHERE);
				return disjunct();
			}
			else if (match(SEMICOLON))
			{
				return null_expression;  // where_clause -> epsilon
			}
			else {
				throw parse_error("Syntax error");
			}
		}

		parse_tree disjunct()
		{
			if (match(L_PAREN)
				|| match(NOT)
				|| match(PLUS)
				|| match(MINUS)
				|| match(NUM)
				|| match(ID))
			{
				// disjunct -> conjunct _disjunct
				parse_tree temp = conjunct();
				parse_tree test = _disjunct();
				if (test.is_null()) {  // test lack a left node
					return temp;
				}
				else {
					test.set_leftmost(temp);
					return test;
				}
			}
			else
			{
				throw parse_error("Syntax error while maching disjunct");
			}
		}

		parse_tree _disjunct()
		{
			if (match(OR))
			{
				// _disjunct -> OR conjunct _disjunct
				consume(OR);
				parse_tree root(OR);

				parse_tree temp = conjunct();
				parse_tree test = _disjunct();
				if (test.is_null()) {
					root.set_right(temp);
					return root;
				}
				else {
					root.set_right(temp);
					test.set_left(root);
					return test;
				}
			}
			else if (match(SEMICOLON) || match(R_PAREN))
			{
				return null_expression;  // _disjunct-> epsilon
			}
			else
			{
				throw parse_error("Syntax error while matching disjunct");
			}
		}


		parse_tree conjunct()
		{
			if (match(L_PAREN)
				|| match(NOT)
				|| match(PLUS)
				|| match(MINUS)
				|| match(NUM)
				|| match(ID))
			{
				// conjunct -> bool _conjunct
				parse_tree temp = boolean();
				parse_tree test = _conjunct();
				if (test.is_null()) {  // test lack a left node
					return temp;
				}
				else {
					test.set_leftmost(temp);
					return test;
				}
			}
			else {
				throw parse_error("Syntax error while matching conjunct");
			}
		}

		parse_tree _conjunct()
		{
			if (match(AND))
			{
				// _conjunct -> AND bool _conjunct
				consume(AND);
				parse_tree root(AND);

				parse_tree temp = boolean();
				parse_tree test = _conjunct();

				if (test.is_null())
				{
					root.set_right(temp);
					return root;
				}
				else
				{
					root.set_right(temp);
					test.set_left(root);
					return test;
				}
			}
			else if (match(OR)
				|| match(SEMICOLON)
				|| match(R_PAREN))
			{
				return null_expression;  // _conjunct -> epsilon
			}
			else
			{
				throw parse_error("Syntax error while matching conjunct");
			}
		}

		parse_tree boolean()
		{
			if (match(NUM)
				|| match(ID)
				|| match(PLUS)
				|| match(MINUS))
			{
				// bool -> comp
				return comp();
			}
			else if (match(L_PAREN))
			{
				// bool -> L_PAREN disjunct R_PAREN
				consume(L_PAREN);
				parse_tree temp = disjunct();
				consume(R_PAREN);
				return temp;
			}
			else if (match(NOT))
			{
				// bool -> NOT bool
				consume(NOT);
				parse_tree temp(NOT);
				temp.set_right(boolean());
				return temp;
			}
			else
			{
				throw parse_error("Syntax error while matching boolean");
			}
		}


		parse_tree comp()
		{
			if (match(NUM)
				|| match(ID)
				|| match(PLUS)
				|| match(MINUS))
			{
				parse_tree temp;
				temp.set_left(expr(false));
				temp.set_type(rop());
				temp.set_right(expr(false));
				return temp;
			}
			else
			{
				throw parse_error("Syntax error while matching comparisons");
			}
		}

		parse_tree expr(bool simple)
		{
			if (match(NUM)
				|| match(PLUS)
				|| match(MINUS)
				|| (simple && match(L_PAREN))
				|| (!simple && match(ID)))
			{
				// expr[] -> term[] _expr[]
				parse_tree temp = term(simple);
				parse_tree test = _expr(simple);
				if (test.is_null())
				{
					return temp;
				}
				else
				{
					test.set_leftmost(temp);
					return test;
				}
			}
			else
			{
				throw parse_error("Syntax error when matching expressions");
			}
		}

		parse_tree _expr(bool simple)
		{
			if (match(PLUS)
				|| match(MINUS))
			{
				// _expr[] -> PLUS term[] _expr[]
				// _expr[] -> MINUS term[] _expr[]
				token_type op = lookahead.get_type();
				consume(op);
				parse_tree root(op);

				parse_tree temp = term(simple);
				parse_tree test = _expr(simple);
				if (test.is_null())
				{
					root.set_right(temp);
					return root;
				}
				else
				{
					root.set_right(temp);
					test.set_left(root);
					return test;
				}
			}
			else if (simple && (match(COMMA) || match(R_PAREN)))
			{
				return null_expression;  // _expr[] -> epsilon
			}
			else if (!simple && (
				match(NEQ)
				|| match(EQ)
				|| match(LT) || match(GT) || match(LEQ)
				|| match(GEQ) || match(AND) || match(OR)
				|| match(SEMICOLON) || match(R_PAREN)))
			{
				return null_expression;  // _expr[] -> epsilon
			}
			else
			{
				throw parse_error("Syntax error while matching expressions");
			}
		}

		parse_tree term(bool simple)
		{
			if (match(NUM)
				|| match(PLUS)
				|| match(MINUS)
				|| (simple && match(L_PAREN))
				|| (!simple && match(ID)))
			{
				// term[] -> unary[] _term[]

				parse_tree temp = unary(simple);
				parse_tree test = _term(simple);
				if (test.is_null()) {  // test lack a left node
					return temp;
				}
				else
				{
					test.set_leftmost(temp);
					return test;
				}
			}
			else
			{
				throw parse_error("Syntax error while matching terms");
			}
		}


		parse_tree _term(bool simple)
		{
			if (match(MUL) || match(DIV))
			{
				// _term[] -> MUL unary[] _term[]
				// _term[] -> DIV unary[] _term[]
				token_type op = lookahead.get_type();
				consume(op);
				parse_tree root(op);

				parse_tree temp = unary(simple);
				parse_tree test = _term(simple);
				if (test.is_null())
				{
					root.set_right(temp);
					return root;
				}
				else
				{
					root.set_right(temp);
					test.set_left(root);
					return test;
				}
			}
			else if (match(PLUS) || match(MINUS))
			{
				return null_expression;  // _term[] -> epsilon
			}
			else if (simple && (match(COMMA) || match(R_PAREN)))
			{
				return null_expression;  // _term[] -> epsilon
			}
			else if (!simple && (match(NEQ) || match(EQ)
				|| match(LT) || match(GT) || match(LEQ)
				|| match(GEQ) || match(AND) || match(OR)
				|| match(SEMICOLON) || match(R_PAREN)))
			{
				return null_expression;  // _term[]-> epsilon
			}
			else
			{
				throw parse_error("Syntax error while matching terms");
			}
		}

		parse_tree unary(bool simple)
		{
			if (match(PLUS) || match(MINUS))
			{
				// unary[] -> PLUS unary[]
				// unary[] -> MINUS unary[]
				token_type op = lookahead.get_type();
				consume(op);
				parse_tree root(op);
				root.set_right(unary(simple));
				return root;
			}
			else if (match(NUM))
			{
				// unary[] ->  num
				int result = num();
				parse_tree temp(NUM);
				temp.set_val(expr_token(NUM, &result, sizeof(result)));
				return temp;
			}
			else if (!simple && match(ID))
			{
				// unary[false] -> id
				string result = id();
				parse_tree temp(ID);
				temp.set_val(expr_token(ID, result.c_str(), result.size()));
				return temp;
			}
			else if (simple && match(L_PAREN))
			{
				// unary[true] -> L_PAREN expr[true] R_PAREN
				consume(L_PAREN);
				parse_tree temp = expr(true);
				consume(R_PAREN);
				return temp;
			}
			else
			{
				throw parse_error("Syntax error while matching unary");
			}
		}

		token_type rop()
		{
			if (match(NEQ) || match(EQ)
				|| match(LT) || match(GT)
				|| match(LEQ) || match(GEQ))
			{
				token_type result = lookahead.get_type();
				consume(result);
				return result;
			}
			else
			{
				throw parse_error("Syntax error while matching relational operators");
			}
		}

		Query query_stmt()

		{
			if (match(SELECT))

			{
				// query_stmt -> SELECT select_list FROM id where_clause SEMICOLON
				consume(SELECT);
				vector<string> columns;
				select_list(columns);
				consume(FROM);
				string table_id = id();
				parse_tree where = where_clause();
				consume(SEMICOLON);
				return Query(table_id, columns, where);
			}
			else

			{
				throw parse_error("Syntax error");
			}
		}

		Parser& select_list(vector<string>& columns)
		{
			if (match(MUL))
			{
				// select_list -> MUL
				columns.push_back("*");
				consume(MUL);
			}
			else if (match(ID))
			{
				// select_list -> column_list
				column_list(columns);
			}
			else
			{
				throw parse_error("Syntax error");
			}
			return *this;
		}

		// терминалы
		string id()
		{
			if (match(ID))
			{
				string result = lookahead.get_id();
				advance();
				return result;
			}
			else
			{
				throw parse_error("Syntax error: Expected ID");
			}
		}

		int num()
		{
			if (match(NUM))
			{
				int result = lookahead.get_number();
				advance();
				return result;
			}
			else
			{
				throw parse_error("Syntax error: Expected number");
			}
		}
		
		// консюмим ключевые слова и операторы
		Parser& consume(token_type t)
		{
			if (match(t))
			{
				advance();
			}
			else
			{
				throw parse_error("Syntax error");
			}
			return *this;
		}

		// Этот метод используем для просмотра вперед и записи строки/столбца
		// TODO: не дергать lexer.next() руками
		void advance()
		{
			if (lookahead == SEMICOLON && !delay)
			{
				delay = true;
			}
			else
			{
				col = lexer.get_column();
				line = lexer.get_line();
				lookahead = lexer.next();
				delay = false;
			}
		}

		// дальше ничего нет == END
		bool is_end() const 
		{
			return lookahead == END;
		}

		int get_column() const 
		{
			return col;
		}

		int get_line() const 
		{
			return line;
		}

		bool match(token_type t) 
		{
			if (delay) 
			{
				advance();
			}
			return lookahead == t;
		}

		Parser& consume_until_start() 
		{
			while (
				!match(CREATE) 
				&& !match(INSERT) 
				&& !match(DELETE)
				&& !match(SELECT) 
				&& !match(END)) 
			{
				advance();
			}
			return *this;
		}

		~Parser() {}

	private:
		expr_token lookahead;
		Lexer& lexer;
		int col;  
		int line;  
		bool delay;
	};

}

