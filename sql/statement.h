﻿#pragma once
#include "parse_tree.h"

#include <set>

using std::multimap;
using std::multiset;


namespace crud 
{

	class Statement 
	{
	public:
		Statement(string _id) : id(_id) {}
		string getId() const 
		{
			return id; 
		}
	private:
		string id;
	};

	/**
	 * Выражение Create. Состоит из id defaults keys.
	 */
	class Create : public Statement 
	{
	public:
		Create(const string _id, const multimap<string, int>& _default_spec,
			const vector<vector<string> >& _keys) :
			Statement(_id),
			default_spec(_default_spec),
			keys(_keys)
		{}

		const multimap<string, int>& get_defaults() const 
		{
			return default_spec;
		}

		const vector<vector<string> >& get_keys() const 
		{
			return keys;
		}

	private:
		multimap<string, int> default_spec;
		vector<vector<string> > keys;
	};

	/**
	 * Insert. Состоит из id columns values(index-paired).
	 */
	class Insert : public Statement 
	{
	public:
		Insert(const string _id, const vector<string>& _columns,
			const vector<int>& _values) :
			Statement(_id),
			columns(_columns),
			values(_values) 
		{}

		const vector<string>& get_columns() const 
		{
			return columns;
		}

		const vector<int>& get_values() const 
		{
			return values;
		}

	private:
		vector<string> columns;
		vector<int> values;
	};

	/**
	 * Delete. Состоит из id и where выражений.
	 */
	class Delete : public Statement 
	{
	public:
		Delete(const string _id, const parse_tree& _where)
			: Statement(_id), where(_where) 
		{}

		const parse_tree& get_where() const 
		{
			return where;
		}
	private:
		parse_tree where;
	};

	/**
	 * Запрос. id, columns,  where .
	 */
	class Query : public Statement 
	{
	public:
		Query(const string _id, const vector<string> _columns,
			const parse_tree& _where)
			: Statement(_id), columns(_columns), where(_where) 
		{}

		const vector<string>& get_columns() const 
		{
			return columns;
		}

		const parse_tree& get_where() const 
		{
			return where;
		}
	private:
		vector<string> columns;  // can contain '*'
		parse_tree where;
	};

}