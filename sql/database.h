﻿#pragma once
#include <iostream>
#include <iomanip>

#include "token.h"
#include "lexer.h"
#include "parser.h"
#include "db_engine.h"
#include "parse_tree.h"



namespace crud {

	constexpr int WIDTH_PER_COL = 15;

	using std::cout;
	using std::setw;

	class database 
	{
	public:
		database(Parser& p, Lexer& l, db_engine& e) : parser(p), lexer(l), engine(e) 
		{
		}
				
		void print_ln(int length, char ch = '-')
		{
			for (int i = 0; i < length; ++i)
			{
				cout << ch;
			}
			cout << '\n';
		}
		// вывод данных и имен столбцов
		void print_data(const vector<string>& names, vector<vector<int> >& results)
		{
			// вывод хедера
			print_ln(names.size() * WIDTH_PER_COL);
			for (const auto& name : names) {
				cout << "|" << setw(WIDTH_PER_COL - 1) << name;
			}
			cout << "|\n";

			// вывод данных
			print_ln(names.size() * WIDTH_PER_COL);
			for (const auto& record : results)
			{
				for (auto col : record)
				{
					cout << "|" << setw(WIDTH_PER_COL - 1) << col;
				}
				cout << "|\n";
				print_ln(names.size() * WIDTH_PER_COL);
			}
		}

		// ивент луп для вычисления выражений
		void event_loop()
		{
			while (!parser.is_end())
			{
				try
				{
					token_type next = parser.check_next_statement();
					execute(next);
				}
				catch (lexer_error e)
				{
					cout << "line " << lexer.get_line() << ", ";
					cout << "column " << lexer.get_column() << ": ";
					cout << e.what() << '\n';
				}
				catch (parse_error e)
				{
					cout << "line " << parser.get_line() << ", ";
					cout << "column " << parser.get_column() << ": ";
					cout << e.what() << '\n';
					parser.consume_until_start();
				}
				catch (db_error e)
				{
					cout << "line " << parser.get_line() << ": ";
					cout << e.what() << '\n';
				}
				catch (parse_tree_error e)
				{
					cout << "line " << parser.get_line() << ", ";
					cout << e.what() << '\n';
					parser.consume_until_start();
				}
			}
		}

		// TODO: вывод ошибок
		void execute(token_type next)
		{
			if (next == CREATE)
			{
				Create create_stmt = parser.create_statement();
				engine.create_(create_stmt);
				cout << "Created table " << create_stmt.getId() << "\n";
			}
			else if (next == INSERT)
			{
				Insert insert_stmt = parser.insert_statement();
				int number = engine.insert_(insert_stmt);
				cout << "Inserted " << number << " rows into table ";
				cout << insert_stmt.getId() << "\n";
			}
			else if (next == DELETE)
			{
				Delete delete_stmt = parser.delete_statement();
				int number = engine.delete_(delete_stmt);
				cout << "Deleted " << number << " rows from table ";
				cout << delete_stmt.getId() << "\n";
			}
			else if
				(next == SELECT)
			{
				Query query_stmt = parser.query_stmt();

				string table_id = query_stmt.getId();
				vector<vector<int> > results;
				vector<string> names = query_stmt.get_columns();

				int number = engine.query_(query_stmt, results);
				if (number > 0)
				{
					// вместо * подставляем имена колонок
					if (std::find(names.begin(), names.end(), "*")
						!= names.end()) {
						names = engine.get_columns(table_id);
					}

					print_data(names, results);

					cout << number << " matching rows in ";
					cout << table_id << "\n";
				}
				else
				{
					cout << "No matching rows in ";
					cout << table_id << "\n";
				}
			}
			else if (next == END)
			{
				return;
			}
		}
	private:
		Parser& parser;
		Lexer& lexer;
		db_engine& engine;
	};

}

