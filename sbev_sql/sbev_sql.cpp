#include <iostream>
#include <fstream>
#include <memory>
#include "database.h"

using std::cin;
using std::ifstream;
using std::ofstream;
using std::unique_ptr;

using namespace crud;

int main(int argc, char const* argv[]) 
{
    //unique_ptr<Lexer> lexptr;
    Lexer* lexptr;
    ofstream out;
    ifstream in;  

    if (argc > 1) 
    {
        in.open(argv[1], ifstream::in);
        if (in.is_open()) {
            lexptr = new Lexer(in);
        }
        else 
        {
            cout << "Fail to open " << argv[1] << '\n';
            exit(1);
        }
    }
    else 
    {
        cout << "Waiting for input..." << "\n" ;
        lexptr = new Lexer(cin);
    }

    Parser parser(*lexptr);
    db_engine engine;
    database io(parser, *lexptr, engine);

    // try-catch in case the first lexeme is invalid
    try 
    {
        io.event_loop();
    }
    catch (lexer_error e) 
    {
        cout << "line " << lexptr->get_line() << ", ";
        cout << "column " << lexptr->get_column() << ": ";
        cout << e.what() << '\n';
    }
    catch (parse_error e)
    {
        cout << "line " << parser.get_line() << ", ";
        cout << "column " << parser.get_column() << ": ";
        cout << e.what() << '\n';
    }
    catch (std::exception e)
    {
        cout << "Unknown error " << '\n';
        cout << e.what() << '\n';
    }

    delete lexptr;
    return 0;
}