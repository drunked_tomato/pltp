#include <iostream>
#include <fstream>

#include "token.h"
#include "lexer.h"
#include "parser.h"
#include "expression.h"

using std::cin;
using std::cout;
using std::ifstream;
using std::ofstream;
using namespace crud;

int main(int argc, char const* argv[])
{
	Lexer* lexptr;
	ofstream out;
	ifstream in; 

	if (argc > 1)
	{
		in.open(argv[1], ifstream::in);
		if (in.is_open()) 
		{
			lexptr = new Lexer(in);
		}
		else
		{
			cout << "Fail to open " << argv[1] << '\n';
			exit(1);
		}
	}
	else
	{
		cout << ">";
		lexptr = new Lexer(cin);
	}

	Parser parser(*lexptr);
	while (!parser.is_end())
	{
		try
		{
			token_type next = parser.check_next_statement();
			if (next == CREATE) {
				Create create = parser.create_statement();

				cout << "Until line: " << parser.get_line();
				cout << ", col " << parser.get_column() << "\n";

				cout << "create ID: " << create.getId() << '\n';

				cout << "create defaults" << '\n';
				const multimap<string, int>& defs = create.get_defaults();
				for (auto it = defs.begin(); it != defs.end(); ++it)
				{
					cout << it->first << ": " << it->second << "\n";
				}

				cout << "create keys:\n";
				const vector<vector<string> >& keys = create.get_keys();
				for (auto key : keys)
				{
					for (auto col : key)
					{
						cout << col << "\n";
					}
				}
				cout << "\n";
			}
			else if (next == INSERT)
			{
				Insert insert = parser.insert_statement();

				cout << "Until line: " << parser.get_line();
				cout << ", col " << parser.get_column() << "\n";

				cout << "insert ID: " << insert.getId() << '\n';

				cout << "insert values" << '\n';
				const vector<string>& columns = insert.get_columns();
				const vector<int>& values = insert.get_values();
				for (int i = 0; i < columns.size(); ++i)
				{
					cout << columns[i] << ": " << values[i] << '\n';
				}
				cout << "\n";
			}
			else if (next == DELETE)
			{
				Delete del = parser.delete_statement();

				cout << "Until line: " << parser.get_line();
				cout << ", col " << parser.get_column() << "\n";

				cout << "delete ID: " << del.getId() << '\n';
				cout << "delete clause" << '\n';
				cout << del.get_where();
				cout << "\n\n";
			}
			else if (next == SELECT)
			{
				Query query = parser.query_stmt();

				cout << "Until line: " << parser.get_line();
				cout << ", col " << parser.get_column() << "\n";

				cout << "query ID: " << query.getId() << '\n';

				cout << "query columns: ";
				const vector<string>& columns = query.get_columns();
				for (int i = 0; i < columns.size(); ++i)
				{
					cout << columns[i] << ' ';
				}

				cout << "\nquery clause" << '\n';
				cout << query.get_where();
				cout << "\n\n";
			}
		}
		catch (lexer_error e)
		{
			cout << lexptr->get_line() << ", " << lexptr->get_column() << ": ";
			cout << e.what() << '\n';
			exit(1);
		}
		catch (parse_error e)
		{
			cout << parser.get_line() << ", " << parser.get_column() << ": ";
			cout << e.what() << '\n';
			exit(1);
		}
	}

	delete lexptr;
	return 0;
}
